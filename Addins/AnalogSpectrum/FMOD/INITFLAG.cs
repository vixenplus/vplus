﻿namespace FMOD
{
    public enum INITFLAG
    {
        _3D_RIGHTHANDED = 2,
        DISABLESOFTWARE = 4,
        DSOUND_HRTFFULL = 0x800,
        DSOUND_HRTFLIGHT = 0x400,
        DSOUND_HRTFNONE = 0x200,
        NORMAL = 0,
        PS2_DISABLECORE0REVERB = 0x10000,
        PS2_DISABLECORE1REVERB = 0x20000,
        STREAM_FROM_UPDATE = 1,
        XBOX_REMOVEHEADROOM = 0x100000
    }
}


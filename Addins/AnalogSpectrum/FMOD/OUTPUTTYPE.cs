﻿using System;

namespace FMOD
{
	public enum OUTPUTTYPE
	{
		AUTODETECT,
		UNKNOWN,
		NOSOUND,
		WAVWRITER,
		NOSOUND_NRT,
		WAVWRITER_NRT,
		DSOUND,
		WINMM,
		ASIO,
		OSS,
		ALSA,
		ESD,
		SOUNDMANAGER,
		COREAUDIO,
		XBOX,
		PS2,
		GC,
		XBOX360,
		PSP,
		OPENAL,
		MAX
	}
}


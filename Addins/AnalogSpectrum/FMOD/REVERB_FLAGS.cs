﻿namespace FMOD
{
    public class REVERB_FLAGS
    {
        public const uint DECAYHFLIMIT = 0x20;
        public const uint DECAYTIMESCALE = 1;
        public const uint DEFAULT = 0x3f;
        public const uint ECHOTIMESCALE = 0x40;
        public const uint MODULATIONTIMESCALE = 0x80;
        public const uint REFLECTIONSDELAYSCALE = 4;
        public const uint REFLECTIONSSCALE = 2;
        public const uint REVERBDELAYSCALE = 0x10;
        public const uint REVERBSCALE = 8;
    }
}


﻿namespace FMOD
{
    public enum SPEAKER
    {
        BACK_CENTER = 3,
        BACK_LEFT = 4,
        BACK_RIGHT = 5,
        FRONT_CENTER = 2,
        FRONT_LEFT = 0,
        FRONT_RIGHT = 1,
        LOW_FREQUENCY = 3,
        MAX = 8,
        MONO = 0,
        SIDE_LEFT = 6,
        SIDE_RIGHT = 7
    }
}


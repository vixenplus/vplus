﻿namespace RemoteClient
{
    using System;

    public enum ExecutionStatus
    {
        None,
        Stopped,
        Running,
        Paused
    }
}


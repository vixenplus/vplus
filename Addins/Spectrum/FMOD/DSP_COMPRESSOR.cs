﻿namespace FMOD
{
    using System;

    public enum DSP_COMPRESSOR
    {
        THRESHOLD,
        ATTACK,
        RELEASE,
        GAINMAKEUP
    }
}


﻿namespace FMOD
{
    using System;

    public enum DSP_ECHO
    {
        DELAY,
        DECAYRATIO,
        MAXCHANNELS,
        DRYMIX,
        WETMIX
    }
}


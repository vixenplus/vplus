﻿namespace FMOD
{
    using System;

    public enum DSP_ITECHO
    {
        WETDRYMIX,
        FEEDBACK,
        LEFTDELAY,
        RIGHTDELAY,
        PANDELAY
    }
}


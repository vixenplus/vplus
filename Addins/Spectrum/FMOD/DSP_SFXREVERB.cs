﻿namespace FMOD
{
    using System;

    public enum DSP_SFXREVERB
    {
        DRYLEVEL,
        ROOM,
        ROOMHF,
        ROOMROLLOFFFACTOR,
        DECAYTIME,
        DECAYHFRATIO,
        REFLECTIONSLEVEL,
        REFLECTIONSDELAY,
        REVERBLEVEL,
        REVERBDELAY,
        DIFFUSION,
        DENSITY,
        HFREFERENCE
    }
}


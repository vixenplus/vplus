﻿namespace FMOD
{
    using System;

    public enum OPENSTATE
    {
        READY,
        LOADING,
        ERROR,
        CONNECTING,
        BUFFERING,
        SEEKING,
        STREAMING
    }
}


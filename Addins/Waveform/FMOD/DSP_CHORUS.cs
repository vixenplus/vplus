﻿namespace FMOD
{
    using System;

    public enum DSP_CHORUS
    {
        DRYMIX,
        WETMIX1,
        WETMIX2,
        WETMIX3,
        DELAY,
        RATE,
        DEPTH,
        FEEDBACK
    }
}


﻿namespace FMOD
{
    using System;

    public enum DSP_ITLOWPASS
    {
        CUTOFF,
        RESONANCE
    }
}


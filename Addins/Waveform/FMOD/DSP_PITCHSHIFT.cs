﻿namespace FMOD
{
    using System;

    public enum DSP_PITCHSHIFT
    {
        PITCH,
        FFTSIZE,
        OVERLAP,
        MAXCHANNELS
    }
}


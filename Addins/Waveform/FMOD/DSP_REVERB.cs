﻿namespace FMOD
{
    using System;

    public enum DSP_REVERB
    {
        ROOMSIZE,
        DAMP,
        WETMIX,
        DRYMIX,
        WIDTH,
        MODE
    }
}


﻿namespace FMOD
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate RESULT DSP_SETPARAMCALLBACK(ref DSP_STATE dsp_state, int index, float val);
}


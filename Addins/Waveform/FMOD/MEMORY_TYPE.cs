﻿namespace FMOD
{
    using System;

    public enum MEMORY_TYPE
    {
        NORMAL = 0,
        PERSISTENT = 0x200000,
        XBOX360_PHYSICAL = 0x100000
    }
}


﻿namespace FMOD
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct VECTOR
    {
        public float x;
        public float y;
        public float z;
    }
}


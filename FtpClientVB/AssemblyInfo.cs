﻿// Assembly FtpClientVB, Version 1.1.0.0

[assembly: System.Reflection.AssemblyVersion("1.1.0.0")]
[assembly: System.Reflection.AssemblyCopyright("Copyright \x00a9 Brackenbury Consulting Limited 2007")]
[assembly: System.Reflection.AssemblyTitle("FtpClientVB")]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
[assembly: System.Reflection.AssemblyCompany("Brackenbury Consulting Limited")]
[assembly: System.Reflection.AssemblyProduct("FtpClientVB")]
[assembly: System.Reflection.AssemblyDescription("VB.NET version")]
[assembly: System.Reflection.AssemblyTrademark("")]
[assembly: System.Diagnostics.Debuggable(System.Diagnostics.DebuggableAttribute.DebuggingModes.DisableOptimizations | System.Diagnostics.DebuggableAttribute.DebuggingModes.EnableEditAndContinue | System.Diagnostics.DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | System.Diagnostics.DebuggableAttribute.DebuggingModes.Default)]
[assembly: System.Runtime.CompilerServices.CompilationRelaxations(8)]
[assembly: System.Runtime.CompilerServices.RuntimeCompatibility(WrapNonExceptionThrows=true)]
[assembly: System.Reflection.AssemblyFileVersion("1.1.0.0")]
[assembly: System.Runtime.InteropServices.Guid("a2e038ba-b8ca-4ff3-8339-d66071eb380b")]


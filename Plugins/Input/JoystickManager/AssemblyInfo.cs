// Assembly JoystickManager, Version 1.0.0.0

[assembly: System.Reflection.AssemblyVersion("0.0.1.180")]
[assembly: System.Reflection.AssemblyFileVersion("0.0.1.180")]
[assembly: System.Runtime.InteropServices.Guid("e54f154d-a8e8-4420-a61a-33d1df78dc43")]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
[assembly: System.Reflection.AssemblyTrademark("")]
[assembly: System.Reflection.AssemblyCopyright("Copyleft 2013")]
[assembly: System.Reflection.AssemblyProduct("JoystickManager")]
[assembly: System.Reflection.AssemblyCompany("")]
[assembly: System.Reflection.AssemblyConfiguration("")]
[assembly: System.Reflection.AssemblyDescription("Vixen+ Lighting Control")]
[assembly: System.Reflection.AssemblyTitle("JoystickManager")]
[assembly: System.Diagnostics.Debuggable(System.Diagnostics.DebuggableAttribute.DebuggingModes.DisableOptimizations | System.Diagnostics.DebuggableAttribute.DebuggingModes.EnableEditAndContinue | System.Diagnostics.DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | System.Diagnostics.DebuggableAttribute.DebuggingModes.Default)]
[assembly: System.Runtime.CompilerServices.CompilationRelaxations(8)]
[assembly: System.Runtime.CompilerServices.RuntimeCompatibility(WrapNonExceptionThrows=true)]


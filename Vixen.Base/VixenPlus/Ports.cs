﻿namespace VixenPlus
{
    public enum Ports
    {
        ExecutionClient = 41403,
        LocalClient = 41405,
        Server = 41401,
        ServerAutoConnect = 41404,
        Web = 41402
    }
}
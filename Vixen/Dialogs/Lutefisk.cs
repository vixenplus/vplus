﻿using System.Windows.Forms;

using VixenPlusCommon.Properties;

namespace VixenPlus.Dialogs
{
    public partial class Lutefisk: Form
    {
        public Lutefisk()
        {
            InitializeComponent();
            Icon = Resources.VixenPlus;
        }
    }
}

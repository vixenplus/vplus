// Assembly VixenControls, Version 1.0.0.0

using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyVersion("0.2.154.1")]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.DisableOptimizations | DebuggableAttribute.DebuggingModes.EnableEditAndContinue | DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | DebuggableAttribute.DebuggingModes.Default)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows=true)]
[assembly: AssemblyTitle("VixenPlus")]
[assembly: CompilationRelaxations(8)]
[assembly: AssemblyCopyright("Copyleft 2013")]
[assembly: AssemblyDescription("Vixen+ Lighting Control")]
[assembly: ComVisible(false)]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyFileVersion("0.2.154.1")]
[assembly: AssemblyProduct("VixenPlus")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyConfiguration("")]


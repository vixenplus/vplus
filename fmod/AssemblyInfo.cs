// Assembly fmod, Version 1.0.0.0

using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyVersion("0.2.154.1")]
[assembly: AssemblyTitle("fmod")]
[assembly: AssemblyDescription("FMOD interface for Vixen")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("VixenPlus")]
[assembly: AssemblyCopyright("Copyleft 2013")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: Guid("69a72273-4e43-4617-a8bf-0ce05435efe3")]
[assembly: AssemblyFileVersion("0.2.154.1")]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.DisableOptimizations | DebuggableAttribute.DebuggingModes.EnableEditAndContinue | DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | DebuggableAttribute.DebuggingModes.Default)]
[assembly: CompilationRelaxations(8)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows=true)]

